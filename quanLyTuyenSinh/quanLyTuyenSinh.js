function tinhDiemKhuVuc() {
  var diemKhuVuc = "";
  var khuVuc = document.getElementById("khuVuc").value;
  if (khuVuc == "A") {
    diemKhuVuc = 2 * 1;
  } else if (khuVuc == "B") {
    diemKhuVuc = 1 * 1;
  } else if (khuVuc == "C") {
    diemKhuVuc = 0.5 * 1;
  } else {
    diemKhuVuc = parseInt(0);
  }
  return diemKhuVuc;
}

function tinhDiemDoiTuong() {
  var doiTuong = document.getElementById("doiTuong").value;
  var diemDoiTuong = "";
  if (doiTuong == 1) {
    diemDoiTuong = 1 * 1;
  } else if (doiTuong == 2) {
    diemDoiTuong = 1 * 2;
  } else if (doiTuong == 3) {
    diemDoiTuong = 1 * 3;
  } else {
    diemDoiTuong = parseInt(0);
  }
  return diemDoiTuong;
}

function quanLyTuyenSinh() {
  var diemChuan = document.getElementById("diemChuan").value * 1;
  var diemMon1 = document.getElementById("diemMon1").value * 1;
  var diemMon2 = document.getElementById("diemMon2").value * 1;
  var diemMon3 = document.getElementById("diemMon3").value * 1;
  var diemUuTien = tinhDiemDoiTuong() + tinhDiemKhuVuc();
  var tongDiem = diemMon1 + diemMon2 + diemMon3 + diemUuTien;
  //   console.log("Tong diem cua ban la: " + tongDiem);
  if (diemMon1 == 0 || diemMon2 == 0 || diemMon3 == 0) {
    document.getElementById("ketQua").innerHTML = document.getElementById(
      "ketQua"
    ).innerText = `Bạn đã rớt do có điểm bằng 0`;
  } else if (tongDiem >= diemChuan) {
    document.getElementById(
      "ketQua"
    ).innerText = `Điểm của bạn là : ${tongDiem}. Bạn đã đậu.`;
  } else {
    document.getElementById("ketQua").innerHTML = document.getElementById(
      "ketQua"
    ).innerText = `Điểm của bạn là: ${tongDiem}. Bạn đã rớt.`;
  }
}
