function tinhTienDien() {
  var hoTen = document.getElementById("hoTen").value;
  var soDienSuDung = document.getElementById("soDienSuDung").value * 1;
  var thanhTien = "";
  if (soDienSuDung <= 50) {
    thanhTien = soDienSuDung * 500; // Tien 50 kM dau tien
  } else if (soDienSuDung > 50 && soDienSuDung < 100) {
    thanhTien = 50 * 500 + (soDienSuDung - 50) * 650; // Tien 50 kM tiep theo
  } else if (soDienSuDung > 100 && soDienSuDung < 200) {
    thanhTien = 50 * 500 + 50 * 650 + (soDienSuDung - 100) * 850; // 100kM tiep theo
  } else if (soDienSuDung > 200 && soDienSuDung < 350) {
    thanhTien = 50 * 500 + 50 * 650 + 100 * 850 + (soDienSuDung - 200) * 1100; // 150 kM tiep theo
  } else {
    thanhTien =
      50 * 500 +
      50 * 650 +
      100 * 850 +
      150 * 1100 +
      (soDienSuDung - 350) * 1300; // tu 350 km tro len
  }
  document.getElementById("ketQua").innerText = `Anh/Chị: ${hoTen} 
  Số tiền điện sử dụng là: ${thanhTien} VNĐ`;
}
